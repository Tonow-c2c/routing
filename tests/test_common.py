import unittest
import vcr

import json

from models.routing_data_matrix import RoutingDataMatrix
from models.vrp_solver import VrpSolver

with open('config.json', 'r') as f:
    config = json.load(f)
    HOST = config['HOST']

class TestRouting(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # check address with https://www.openstreetmap.org
        # Create data
        # location params
        cls.add1 = '15 Rue Basse Monaco'
        cls.add2 = '1 Avenue des Pins Monaco'
        cls.add3 = """ Maison de Stéphanie de Monaco, Avenue Saint-Martin,
        Monaco-Ville, Monaco, 98000 """
        cls.locations = [cls.add1, cls.add2, cls.add3]

        cls.address_double_location = 'Musée Océanographique Monaco'
        cls.location_error = [cls.add1, cls.address_double_location]

        # routing params
        cls.num_vehicles = 1
        cls.time_max = 2

        # Retrive coordinates and distance and duration infos
        # with Nominatim and OSRM
        cls.routing_data_matrix = RoutingDataMatrix(host=HOST)
        with vcr.use_cassette('vcr_cassettes/coordinate_infos.yaml'):
            cls.coordinates, cls.points = cls.routing_data_matrix.coordinate_infos(
                cls.locations
            )
        with vcr.use_cassette('vcr_cassettes/distance_duration_matrix_simple_route.yaml'):
            cls.distance_matrix, cls.duration_matrix = cls.routing_data_matrix.distance_duration_matrix_simple_route(cls.points)

        # Use VrpSolver
        cls.vrp_solver = VrpSolver(cls.num_vehicles)
        cls.route_0 = cls.vrp_solver.solver_guided_local_search(
            cls.distance_matrix, cls.time_max
        )[0]
        cls.total_distance = cls.vrp_solver.compute_total_distance(
            cls.route_0, cls.distance_matrix
        )

        cls.test_distance_matrix = [
            [0, 10, 20, 30, 11],
            [10, 0, 50, 60, 70],
            [20, 50, 0, 80, 90],
            [30, 60, 80, 0, 12],
            [11, 70, 90, 12, 0]
        ]

        cls.middle_distance_matrix = [
            [0, 2829.6, 743.3, 360, 100.9],
            [3268.2, 0, 2838.5, 2455.1, 3369.1],
            [429.8, 3259.4, 0, 789.7, 530.7],
            [813.1, 2614.9, 383.3, 0, 914],
            [1072.2, 2728.7, 642.4, 259.1, 0]
        ]

        cls.big_distance_matrix = [
            [0, 548, 776, 696, 582, 274, 502, 194, 308, 194, 536, 502, 388, 354, 468, 776, 662],
            [548, 0, 684, 308, 194, 502, 730, 354, 696, 742, 1084, 594, 480, 674, 1016, 868, 1210],
            [776, 684, 0, 992, 878, 502, 274, 810, 468, 742, 400, 1278, 1164, 1130, 788, 1552, 754],
            [696, 308, 992, 0, 114, 650, 878, 502, 844, 890, 1232, 514, 628, 822, 1164, 560, 1358],
            [582, 194, 878, 114, 0, 536, 764, 388, 730, 776, 1118, 400, 514, 708, 1050, 674, 1244],
            [274, 502, 502, 650, 536, 0, 228, 308, 194, 240, 582, 776, 662, 628, 514, 1050, 708],
            [502, 730, 274, 878, 764, 228, 0, 536, 194, 468, 354, 1004, 890, 856, 514, 1278, 480],
            [194, 354, 810, 502, 388, 308, 536, 0, 342, 388, 730, 468, 354, 320, 662, 742, 856],
            [308, 696, 468, 844, 730, 194, 194, 342, 0, 274, 388, 810, 696, 662, 320, 1084, 514],
            [194, 742, 742, 890, 776, 240, 468, 388, 274, 0, 342, 536, 422, 388, 274, 810, 468],
            [536, 1084, 400, 1232, 1118, 582, 354, 730, 388, 342, 0, 878, 764, 730, 388, 1152, 354],
            [502, 594, 1278, 514, 400, 776, 1004, 468, 810, 536, 878, 0, 114, 308, 650, 274, 844],
            [388, 480, 1164, 628, 514, 662, 890, 354, 696, 422, 764, 114, 0, 194, 536, 388, 730],
            [354, 674, 1130, 822, 708, 628, 856, 320, 662, 388, 730, 308, 194, 0, 342, 422, 536],
            [468, 1016, 788, 1164, 1050, 514, 514, 662, 320, 274, 388, 650, 536, 342, 0, 764, 194],
            [776, 868, 1552, 560, 674, 1050, 1278, 742, 1084, 810, 1152, 274, 388, 422, 764, 0, 798],
            [662, 1210, 754, 1358, 1244, 708, 480, 856, 514, 468, 354, 844, 730, 536, 194, 798, 0],
        ]
