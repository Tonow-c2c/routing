from .test_common import TestRouting

class TestConverteMethods(TestRouting):

    def test_coordinate_infos(self):
        self.assertEqual(len(self.coordinates), len(self.locations))
        self.assertEqual(len(self.points), len(self.locations))

    def test_exeption_for_location_multi_nominatime_result(self):
        try:
            # test with address who return 2 location possible
            # So ValueError should raise
            self.coordinates, self.points = self.routing_data_matrix.coordinate_infos(
                self.location_error
            )
        except ValueError as valueexept:
            loc_error_exeption = valueexept
        self.assertEqual(loc_error_exeption.args[0],'To many address')
        self.assertEqual(loc_error_exeption.args[1], 2)
        self.assertEqual(loc_error_exeption.args[2][0].get('type'), 'museum')

    def test_exeption_for_location_dont_match_with_nominatime_result(self):
        address_wrong_location = '$$$ ~~~'
        location_error = [self.add1, address_wrong_location]
        try:
            # test with address who return any location
            # So ValueError should raise
            self.coordinates, self.points = self.routing_data_matrix.coordinate_infos(
                location_error
            )
        except ValueError as valueexept:
            loc_error_exeption = valueexept
        self.assertEqual(loc_error_exeption.args[0],'Address dont match in nominatim')
        self.assertEqual(loc_error_exeption.args[1], 0)
        self.assertEqual(loc_error_exeption.args[2], '$$$ ~~~')

    def test_create_distance_matrix_simple_route(self):
        self.assertEqual(len(self.distance_matrix), len(self.locations))
        # the diag should be null
        self.assertEqual(self.distance_matrix[0][0], 0)
        self.assertEqual(self.distance_matrix[1][1], 0)
        self.assertEqual(self.distance_matrix[2][2], 0)
        # this case in car for '15 Rue Basse Monaco'/'1 Avenue des Pins Monaco'
        # the both direction have not the same distance
        self.assertNotEqual(
            self.distance_matrix[0][1], self.distance_matrix[1][0]
        )
