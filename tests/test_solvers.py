from models.vrp_solver import VrpSolver
from .test_common import TestRouting

class TestSolvers(TestRouting):

    def test_show_logs(self):
        route = self.vrp_solver.solver_guided_local_search(
            self.distance_matrix, self.time_max, show_log=True
        )[0]
        self.total_distance = self.vrp_solver.compute_total_distance(
            route, self.distance_matrix
        )
        self.assertTrue(self.total_distance)

    def test_solver_guided_local_search_default_guided_local_search(self):
        # route_0 is the path from the first location
        # to the first location include
        self.assertEqual(len(self.route_0), len(self.locations) + 1)
        self.assertEqual(self.route_0[0], self.route_0[-1])
        self.assertNotIn(self.route_0[1], self.route_0[2:])
        self.assertNotIn(self.route_0[-2], self.route_0[:-2])
        # create other not optimized route : route_bis
        # with permutation of the 2 first element
        route_bis = self.route_0[3:]
        route_bis.insert(0, self.route_0[1])
        route_bis.insert(0, self.route_0[2])
        route_bis.insert(0, self.route_0[0])
        total_distance_bis = self.vrp_solver.compute_total_distance(
            route_bis, self.distance_matrix
        )
        self.assertLess(self.total_distance, total_distance_bis)

    def test_solver_guided_local_search_localsearchmetaheuristic(self):
        route_greedy_descent = self.vrp_solver.solver_guided_local_search(
            self.distance_matrix,
            self.time_max,
            heuristic_type="LocalSearchMetaheuristic",
            heuristic="GUIDED_LOCAL_SEARCH"
        )[0]
        self.total_distance = self.vrp_solver.compute_total_distance(
            route_greedy_descent, self.distance_matrix
        )
        self.assertEqual(len(route_greedy_descent), len(self.locations) + 1)
        self.assertEqual(route_greedy_descent[0], route_greedy_descent[-1])
        self.assertNotIn(route_greedy_descent[1], route_greedy_descent[2:])
        self.assertNotIn(route_greedy_descent[-2], route_greedy_descent[:-2])
        # create other not optimized route : route_bis
        # with permutation of the 2 first element
        route_bis = route_greedy_descent[3:]
        route_bis.insert(0, route_greedy_descent[1])
        route_bis.insert(0, route_greedy_descent[2])
        route_bis.insert(0, route_greedy_descent[0])
        total_distance_bis = self.vrp_solver.compute_total_distance(
            route_bis, self.distance_matrix
        )
        self.assertLess(self.total_distance, total_distance_bis)

    def test_solver_guided_local_search_localsearchmetaheuristic_big_matix(self):
        route_greedy_descent = self.vrp_solver.solver_guided_local_search(
            self.big_distance_matrix,
            self.time_max,
            heuristic_type="LocalSearchMetaheuristic",
            heuristic="GUIDED_LOCAL_SEARCH"
        )[0]
        self.total_distance = self.vrp_solver.compute_total_distance(
            route_greedy_descent, self.big_distance_matrix
        )
        self.assertEqual(len(route_greedy_descent), len(self.big_distance_matrix) + 1)
        self.assertEqual(route_greedy_descent[0], route_greedy_descent[-1])
        self.assertNotIn(route_greedy_descent[1], route_greedy_descent[2:])
        self.assertNotIn(route_greedy_descent[-2], route_greedy_descent[:-2])
        # create other not optimized route : route_bis
        # with permutation of the 2 first element
        route_bis = route_greedy_descent[3:]
        route_bis.insert(0, route_greedy_descent[1])
        route_bis.insert(0, route_greedy_descent[2])
        route_bis.insert(0, route_greedy_descent[0])
        total_distance_bis = self.vrp_solver.compute_total_distance(
            route_bis, self.big_distance_matrix
        )
        self.assertLess(self.total_distance, total_distance_bis)

    def test_solver_guided_local_search_wrong_heuristic(self):
        try:
            self.vrp_solver.solver_guided_local_search(
                self.distance_matrix, self.time_max, heuristic="SPAN_AND_EGGS"
            )[0]
        except AttributeError as valueexept:
            wrong_heuristic_exeption = valueexept
        self.assertEqual(wrong_heuristic_exeption.args[0],'SPAN_AND_EGGS')
