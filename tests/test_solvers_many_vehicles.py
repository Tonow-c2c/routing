from models.vrp_solver import VrpSolver
from .test_common import TestRouting

class TestSolvers(TestRouting):

    def test_solver_path_cheapest_arc_with_many_vehicles_test_matrix_max_travel_distance_to_low(self):
        # route_0 is the path from the first location
        # to the first location include
        for num_vehicles in range(1,5):
            self.vrp_solver = VrpSolver(num_vehicles)
            routes = self.vrp_solver.solver_guided_local_search(
                self.test_distance_matrix, time_max=4, max_travel_distance=10
            )
            self.assertFalse(routes)

    def test_solver_path_cheapest_arc_with_many_vehicles_test_matrix_max_travel_distance_height(self):
        # route_0 is the path from the first location
        # to the first location include
        for num_vehicles in range(1,5):
            self.vrp_solver = VrpSolver(num_vehicles)
            routes = self.vrp_solver.solver_guided_local_search(
                self.test_distance_matrix, time_max=4, max_travel_distance=1000
            )
            self.assertTrue(routes)
            self.assertEqual(len(routes), num_vehicles)

    def test_solver_path_cheapest_arc_with_many_vehicles_big_matrix_max_dist_not_set(self):
        for num_vehicles in range(2,5):
            self.vrp_solver = VrpSolver(num_vehicles)
            routes = self.vrp_solver.solver_guided_local_search(
                self.big_distance_matrix, time_max=4,
            )
            for route in routes:
                total_distance = self.vrp_solver.compute_total_distance(
                    route, self.big_distance_matrix
                )
                self.assertTrue(route)
                self.assertTrue(total_distance)

    def test_solver_guided_local_search_with_many_vehicles_middle_matrixmax_dist_not_set(self):
        num_vehicles = 2
        self.vrp_solver = VrpSolver(num_vehicles)
        routes = self.vrp_solver.solver_guided_local_search(
            self.middle_distance_matrix, time_max=8
        )
        self.assertEqual(len(routes), num_vehicles)
        for route in routes:
            self.assertTrue(route)
